# A.24 METHOD

**Method** adalah fungsi yang menempel pada struct, sehingga hanya bisa di akses lewat variabel objek.
Keunggulan method dibanding fungsi biasa adalah memiliki akses ke property struct hingga level private. Dan juga, dengan menggunakan method sebuah proses bisa di-enkapsulasi dengan baik.


## Method Pointer

**Method pointer** adalah method yang variabel objek pemilik method tersebut berupa pointer.

Kelebihan method jenis ini adalah, ketika kita melakukan manipulasi nilai pada property lain yang masih satu struct, nilai pada property tersebut akan di rubah pada reference nya.
