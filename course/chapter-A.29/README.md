# A.29 GOROUTINE

Goroutine mirip dengan thread thread, tapi sebenarnya bukan. Sebuah native thread bisa berisikan sangat banyak goroutine. Mungkin lebih pas kalau goroutine disebut sebagai **mini thread**. Goroutine sangat ringan, hanya dibutuhkan sekitar **2kB** memori saja untuk satu buah goroutine. Ekseksui goroutine bersifat asynchronous, menjadikannya tidak saling tunggu dengan goroutine lain.

Karena goroutine sangat ringan, maka eksekusi banyak goroutine bukan masalah. Akan tetapi jika jumlah goroutine sangat banyak sekali (contoh 1 juta goroutine dijalankan pada komputer dengan RAM terbatas), memang proses akan jauh lebih cepat selesai, tapi memory / RAM juga bisa bengkak.

Goroutine merupakan salah satu bagian paling penting dalam **Concurrent Programming** di Golang. Salah satu yang membuat goroutine sangat istimewa adalah eksekusi-nya dijalankan di multi core processor. Kita bisa tentukan berapa banyak core yang aktif, makin banyak akan makin cepat.

Concurrency atau konkurensi berbeda dengan paralel. Paralel adalah eksekusi banyak proses secara bersamaan. Sedangkan konkurensi adalah komposisi dari sebuah proses. Konkurensi merupakan struktur, sedangkan paralel adalah bagaimana eksekusinya berlangsung.

## runtime.GOMAXPROCS()
Fungsi ini digunakan untuk menentukan jumlah core atau processor yang digunakan dalam eksekusi program.

Jumlah yang diinputkan secara otomatis akan disesuaikan dengan jumlah asli logical processor yang ada. Jika jumlahnya lebih, maka dianggap menggunakan sejumlah prosesor yang ada.

## fmt.Scanln()
Fungsi ini akan meng-capture semua karakter sebelum user menekan tombol enter, lalu menyimpannya pada variabel.

## Jenis-jenis programming
- **Concurrent Programming** adalah program merupakan sekumpulan proses yang bekerjasama, saling berbagi informasi dari waktu ke waktu tapi biasanya beroperasi secara tidak serempak.
- **Imperative programming** program terdiri dari instruksi yang membentuk perhitungan, menerima input dan menghasilkan output. Contoh bahasa: Fortran, C, dan C++.
- **Object-oriented (OO) programming** program adalah kumpulan objek yang saling berinteraksi melalui pesan yang mengubah state mereka. Contoh bahasa: Java, C++.
- **Functional programming** program merupakan kumpulan fungsi matematika dengan input (domain) dan hasil (range). Fungsi-fungsi saling berinteraksi dan berkombinasi mengggunakan komposisi fungsional, kondisional, dan rekursif. Contoh bahasa: Lisp, Scheme,ML.
- **Logic (declarative) programming** memodelkan masalah menggunakan bahasa deklaratif, yang terdiri dari fakta dan aturan. Contoh bahasa : Prolog
- **Event-driven programming** program merupakan sebuah loop yang secara kontinu  merespon event yang timbul oleh perintah yang tidak terduga.  Event ini berasal dari aksi user pada layar atau sumber lainnya. Contoh bahasa: Visual Basic dan Java.
