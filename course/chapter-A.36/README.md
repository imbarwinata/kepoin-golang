# A.36 PANIC
**Panic** digunakan untuk menampilkan trace error sekaligus menghentikan flow goroutine (ingat, main juga merupakan goroutine). Setelah ada panic, proses selanjutnya tidak di-eksekusi (kecuali proses yang di-defer, akan tetap dijalankan tepat sebelum panic muncul).

Panic memunculkan pesan di console, sama seperti fmt.Println() hanya saja informasi yang ditampilkan sangat mendetail dan heboh.

Pada program yang telah kita buat tadi, ubah fmt.Println() yang berada di dalam blok kondisi else pada fungsi main menjadi panic(), lalu tambahkan fmt.Println() setelahnya.
