# A.23 STRUCT

Struct adalah kumpulan definisi variabel (atau property) dan atau fungsi (atau method), yang dibungkus dengan nama tertentu.


## CATATAN
- Variabel bertipe pointer ditandai dengan adanya tanda asterisk (*) tepat sebelum penulisan tipe data ketika deklarasi.
- Nilai default variabel pointer adalah nil (kosong). 
- Variabel pointer tidak bisa menampung nilai yang bukan pointer, dan sebaliknya variabel biasa tidak bisa menampung nilai pointer.
- Variabel biasa sebenarnya juga bisa diambil nilai pointernya, caranya dengan menambahkan tanda ampersand (&) tepat sebelum nama variabel. Metode ini disebut dengan referencing.
- Dan sebaliknya, nilai asli variabel pointer juga bisa diambil, dengan cara menambahkan tanda asterisk (*) tepat sebelum nama variabel. Metode ini disebut dengan dereferencing.


# Embedded Struct

- **Embedded** struct adalah mekanisme untuk menyimpan objek cetakan struct kedalam properti sebuah struct lain.
- Embedded struct adalah **mutable**, nilai property-nya nya bisa diubah.

# Anonymous Struct
- **Anonymous Struct** adalah adalah struct yang tidak dideklarasikan di awal, melainkan ketika dibutuhkan saja, langsung pada saat penciptaan objek. Teknik ini cukup efisien untuk pembuatan variabel objek yang struct nya hanya dipakai sekali.