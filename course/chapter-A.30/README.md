# A.30 CHANNEL
**Channel** digunakan untuk menghubungkan gorutine satu dengan goroutine lain. Mekanismenya adalah serah-terima data lewat channel tersebut. Goroutine pengirim dan penerima harus berada pada channel yang berbeda (konsep ini disebut **buffered channel**). Pengiriman dan penerimaan data pada channel bersifat **blocking** atau **synchronous**.

## Eksekusi Goroutine Pada IIFE (Immediately-Invoked Function Expression)
Eksekusi goroutine tidak harus pada fungsi atau closure yang sudah terdefinisi. Sebuah IIFE juga bisa dijalankan sebagai goroutine baru. Caranya dengan langsung menambahkan keyword go pada waktu deklarasi-eksekusi IIFE-nya.
Untuk lebih jelas mengenai IIFE cek kembali pembelajaran fungsi closure
