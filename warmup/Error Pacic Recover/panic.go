package main

import (
  "fmt"
  "strings"
  "errors"
)

func validate(input string) (bool, error) {
  if strings.TrimSpace(input) == "" {
    return false, errors.New("cannot be empty")
  }
  return true, nil
}

func catch() {
  // Recover berguna untuk meng-handle panic error.
if r := recover(); r != nil {
        fmt.Println("Error occured", r)
    } else {
        fmt.Println("Application running perfectly")
    }
}

func main() {
    // Recover berguna untuk meng-handle panic error.
    defer catch()

    var name string
    fmt.Print("Type your name: ")
    fmt.Scanln(&name)

    if valid, err := validate(name); valid {
        fmt.Println("halo", name)
    } else {
      fmt.Println("end")
        panic(err.Error())
    }
}
