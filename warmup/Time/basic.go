package main

import "fmt"
import "time"

func main(){
	var time1 = time.Now()
	fmt.Printf("time1 %v\n", time1)
	// time1 2018-11-06 16:12:11.115512506 +0700 WIB m=+0.000290004

	var time2 = time.Date(2011, 12, 24, 10, 20, 0, 0, time.UTC)
  fmt.Printf("time2 %v\n", time2)
	// time2 2011-12-24 10:20:00 +0000 UTC

	// menampilkan bulan dan tahun dari variable time1
	fmt.Printf("Month time1 %v\n", time1.Month())
	fmt.Printf("Year time1 %v\n", time1.Year())
	// Beberapa method yang tersedia : Month(), Year(), YearDay(), Weekday(), ISOWeek(), Day(), Hour()
																	// Minute(), Second(), Nanosecond(), Local(), Location(), Zone()
																	// IsZero(), UTC(), Unix(), UnixNano(), String()

	
}
