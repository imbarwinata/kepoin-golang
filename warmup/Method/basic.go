package main

import "fmt"
import "strings"

type student struct {
	name string
	age  int
}

func main() {
	var s1 = student{"Imbar Winata", 23}
	s1.sayHello()

	var name = s1.getNameAt(2)
	fmt.Println("nama panggilan :", name)
}

func (s student) sayHello() {
	fmt.Println("halo", s.name)
}

func (s student) getNameAt(i int) string {
	return strings.Split(s.name, " ")[i-1]
}
