package main

import (
	f "fmt"
  m "math"
)

type hitung interface {
	luas() float64
	keliling() float64
}

type lingkaran struct {
  diameter float64
}
func (l lingkaran) jariJari() float64 {
  return l.diameter / 2
}
func (l lingkaran) luas() float64 {
  return m.Pi * m.Pow(l.jariJari(), 2)
}
func (l lingkaran) keliling() float64 {
  return m.Pi * l.diameter
}

type persegi struct {
  sisi float64
}
func (p persegi) luas() float64 {
  return m.Pow(p.sisi, 2)
}
func (p persegi) keliling() float64 {
  return p.sisi * 4
}

func main() {
  // declare interface "hitung" nama interface
  // interface hitung berisi 2 method : luas dan keliling
  var bangunDatar hitung
  bangunDatar = persegi{10.0}
  f.Println("===== persegi")
  f.Println("luas      :", bangunDatar.luas())
  f.Println("keliling  :", bangunDatar.keliling())
  //
  bangunDatar = lingkaran{14.0}
  f.Println("===== lingkaran")
  f.Println("luas      :", bangunDatar.luas())
  f.Println("keliling  :", bangunDatar.keliling())
  f.Println("jari-jari :", bangunDatar.(lingkaran).jariJari())
}
