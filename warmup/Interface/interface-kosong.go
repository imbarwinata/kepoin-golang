package main

import f "fmt"

func main() {
  var secret interface{}

  secret = "Imbar Winata"
  f.Println(secret)

  secret = []string{"apple", "manggo", "banana"}
  f.Println(secret)

  secret = 12.4
  f.Println(secret)
}
