package main

import "fmt"

type student struct {
	name  string
	grade int
}

func main() {
	var s1 = student{}
	s1.name = "Imbar Winata S1"

	var s2 = student{"Imbar Winata S2", 20}

	var s3 = student{name: "Imbar Winata S3"}

	fmt.Println("Name S1 : ", s1.name)
	fmt.Println("Name S2 : ", s2.name)
	fmt.Println("Name S3 : ", s3.name)
}
