package main

import "fmt"

type person struct { name string; age int; grade int }

func main() {
	var s1 = person{ "Imbar Winata", 23, 20 }
	fmt.Println("Name = ", s1.name)
}
