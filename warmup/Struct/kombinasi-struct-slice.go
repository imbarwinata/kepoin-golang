package main

import "fmt"

type person struct {
	name string
	age  int
}

func main() {
	// struct and slice
	var allStudents = []person{
		{name: "Imbar Winata", age: 23},
		{name: "Imbar Winata", age: 16},
	}

	for _, student := range allStudents {
		fmt.Println(student.name, "age is", student.age)
	}

	// struct and slice anonymous
	var cars = []struct {
		merk string
		tipe string
	}{
		{merk: "Honda", tipe: "AT"},
		{merk: "Daihatsu", tipe: "AMT"},
	}
	fmt.Println("")
	for _, car := range cars {
		fmt.Println(car.merk, " and tipe", car.tipe)
	}
}
