package main

import "fmt"

type student struct {
	name  string
	grade int
}

func main() {
	var s1 student
	s1.name = "Imbar Winata"
	s1.grade = 12

	fmt.Println("Name : ", s1.name)
	fmt.Println("Age : ", s1.grade)
}
