package main

import (
	. "belajar-golang-level-akses/library"
	f "fmt" // use alias
)

func main() {
	var s1 = Student{"Imbar Winata", 23}
	f.Println("name ", s1.Name)
	f.Println("grade", s1.Grade)
}
