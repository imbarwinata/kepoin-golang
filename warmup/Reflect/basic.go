package main

import f "fmt"
import "reflect"

func main() {
  var number = 23
  var reflectValue = reflect.ValueOf(number)

  // mengembalikan inlai interface kosong atau interface{}
  f.Println("tipe variabel :", reflectValue.Type())
  f.Println("nilai variabel :", reflectValue.Interface())

  if reflectValue.Kind() == reflect.Int {
    f.Println("nilai variabel :", reflectValue.Int())
  }
}
