package main

import "fmt"
import "reflect"

type student struct {
    Name  string
    Grade int
}

func (s *student) SetName(name string, grade int) {
    s.Name = name
    s.Grade = grade
}

func main() {
    var s1 = &student{Name: "john wick", Grade: 2}
    fmt.Println("nama :", s1.Name)
    fmt.Println("grade :", s1.Grade)

    var reflectValue = reflect.ValueOf(s1)
    var method = reflectValue.MethodByName("SetName")
    method.Call([]reflect.Value{
      reflect.ValueOf("wick"),
      reflect.ValueOf(3),
    })

    fmt.Println("nama :", s1.Name)
    fmt.Println("grade :", s1.Grade)
}
